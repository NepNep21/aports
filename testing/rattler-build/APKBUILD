# Contributor: Orhun Parmaksız <orhunparmaksiz@gmail.com>
# Maintainer: Orhun Parmaksız <orhunparmaksiz@gmail.com>
pkgname=rattler-build
pkgver=0.14.0
pkgrel=0
pkgdesc="A fast conda-package builder"
url="https://github.com/prefix-dev/rattler-build"
arch="all"
license="BSD-3-Clause"
depends="
	bzip2
	xz
	"
makedepends="
	cargo
	cargo-auditable
	openssl-dev
	"
checkdepends="
	patchelf
	git
	"
subpackages="
	$pkgname-doc
	$pkgname-bash-completion
	$pkgname-zsh-completion
	$pkgname-fish-completion
	"
options="net"
source="$pkgname-$pkgver.tar.gz::https://github.com/prefix-dev/rattler-build/archive/v$pkgver.tar.gz"

prepare() {
	default_prepare
	cargo fetch --target="$CTARGET" --locked
	mkdir -p completions/
}

build() {
	cargo auditable build --frozen --release
	local _completion="target/release/$pkgname completion"
	$_completion --shell bash > "completions/$pkgname"
	$_completion --shell fish > "completions/$pkgname.fish"
	$_completion --shell zsh  > "completions/_$pkgname"
}

check() {
	cargo test --frozen -- --skip "test_host_git_source"
}

package() {
	install -Dm 755 "target/release/$pkgname" -t "$pkgdir/usr/bin"
	install -Dm 644 README.md -t "$pkgdir/usr/share/doc/$pkgname"
	install -Dm 644 LICENSE -t "$pkgdir/usr/share/licenses/$pkgname"
	install -Dm 664 "completions/$pkgname" -t "$pkgdir/usr/share/bash-completion/completions/"
	install -Dm 664 "completions/$pkgname.fish" -t "$pkgdir/usr/share/fish/vendor_completions.d/"
	install -Dm 664 "completions/_$pkgname" -t "$pkgdir/usr/share/zsh/site-functions/"
}

sha512sums="
37dafc5bc03def252f69a711175c0e1512773660f1594a14e8a3cf00e69c768a03f881946ab1bc4a9bcf4480dd0ec31f9491c1c8093d31b13dcf340af7a400c1  rattler-build-0.14.0.tar.gz
"
