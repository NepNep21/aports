# Maintainer: Will Sinatra <wpsinatra@gmail.com>
pkgname=font-iosevka
pkgver=29.0.5
pkgrel=0
pkgdesc="Versatile typeface for code, from code"
url="https://typeof.net/Iosevka/"
arch="noarch"
options="!check" # no testsuite
license="OFL-1.1"
depends="fontconfig"
subpackages="
	$pkgname-base
	$pkgname-aile
	$pkgname-etoile
	$pkgname-slab
	$pkgname-curly
	$pkgname-curly-slab:curly_slab
	"
source="
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/PkgTTC-Iosevka-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/PkgTTC-IosevkaAile-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/PkgTTC-IosevkaEtoile-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/PkgTTC-IosevkaSlab-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/PkgTTC-SGr-IosevkaCurly-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/PkgTTC-SGr-IosevkaCurlySlab-$pkgver.zip
	"

builddir="$srcdir"

package() {
	depends="
		$pkgname-base=$pkgver-r$pkgrel
		$pkgname-aile=$pkgver-r$pkgrel
		$pkgname-etoile=$pkgver-r$pkgrel
		$pkgname-slab=$pkgver-r$pkgrel
		$pkgname-curly=$pkgver-r$pkgrel
		$pkgname-curly-slab=$pkgver-r$pkgrel
		"

	install -Dm644 "$builddir"/*.ttc \
		-t "$pkgdir"/usr/share/fonts/${pkgname#font-}
}

base() {
	pkgdesc="$pkgdesc (Iosevka)"
	amove usr/share/fonts/iosevka/Iosevka-*.ttc
}

aile() {
	pkgdesc="$pkgdesc (Iosevka Aile)"
	amove usr/share/fonts/iosevka/IosevkaAile-*.ttc
}

etoile() {
	pkgdesc="$pkgdesc (Iosevka Etoile)"
	amove usr/share/fonts/iosevka/IosevkaEtoile-*.ttc
}

slab() {
	pkgdesc="$pkgdesc (Iosevka Slab)"
	amove usr/share/fonts/iosevka/IosevkaSlab-*.ttc
}

curly() {
	pkgdesc="$pkgdesc (Iosevka Curly)"
	amove usr/share/fonts/iosevka/SGr-IosevkaCurly-*.ttc
}

curly_slab() {
	pkgdesc="$pkgdesc (Iosevka Curly Slab)"
	amove usr/share/fonts/iosevka/SGr-IosevkaCurlySlab-*.ttc
}

sha512sums="
82ad46aa6cb15cce579a057500509d4c9f27a5b81a239d4ce2dd7bc6cb724c7bb4c76559a3dd8610f4cf3250b1265948ad79e7037a88839b696034594331556e  PkgTTC-Iosevka-29.0.5.zip
292558fc8c690f7fe2736e538b8e3eac12085cee49a450ea205d0389ea177d06e911df54d791bc7d4f3a3979390123d7ceac622ea231f006786f57969aac188c  PkgTTC-IosevkaAile-29.0.5.zip
591f378a01300bba01f0a71ecdba6bf73f3dd70bb926c5825ff24ccf3a2de1d7cc220d95d4d9f4f2823366657e0b5260f072850f566ba32fa69eebe8211feb26  PkgTTC-IosevkaEtoile-29.0.5.zip
e96197ca8d174d63000b09d4d1c17b773cd0fd4d82bb13256934eccfcf91d390bf2fd8c10c3df81d47fb9b0732aa4f21bf28f0046f4b367114902c6fdb819eac  PkgTTC-IosevkaSlab-29.0.5.zip
ce633d7cdd464a31871e07b5c06b63eea8263cdeac439bd5d8744f60f8cbcf03cd490bfd3c19ce1e6aef23d551c0bdd4aa7321fce720dd7dd50ee8b313289ab9  PkgTTC-SGr-IosevkaCurly-29.0.5.zip
14ce4b06f12e8ac2d134e3bed3950efca5f2343d9832bcfcac04e2fc9522917c1628c917735d3a90e993a93a570263da3e749a316bd1b37d1a9b7be6702e3eb7  PkgTTC-SGr-IosevkaCurlySlab-29.0.5.zip
"
