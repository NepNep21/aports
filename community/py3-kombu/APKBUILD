# Contributor: Stefan Wagner <stw@bit-strickerei.de>
# Maintainer: Antoine Martin (ayakael) <dev@ayakael.net>
pkgname=py3-kombu
pkgver=5.3.6
pkgrel=0
pkgdesc="a message queue abstraction layer"
options="!check" # 3 Redis tests fail
url="https://pypi.org/project/kombu/"
arch="noarch !s390x" # Limited by py3-dill
license="BSD-3-Clause"
depends="py3-amqp py3-vine"
makedepends="py3-setuptools py3-gpep517 py3-wheel"
checkdepends="py3-pyro4 py3-case py3-nose py3-mock py3-tz py3-pytest py3-sqlalchemy py3-fakeredis"
subpackages="$pkgname-pyc"
source="https://files.pythonhosted.org/packages/source/k/kombu/kombu-$pkgver.tar.gz"
builddir="$srcdir/kombu-$pkgver"

replaces="py-kombu" # Backwards compatibility
provides="py-kombu=$pkgver-r$pkgrel" # Backwards compatibility

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 setup.py test
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl
}

sha512sums="
22712b3f105a87790103e8df3565d9bfa30bfadc706d7f8d8dc7eb908575603b8f3f596cc04b428bd69ff63b484509aadf0f031a792e315883d3dc878705a79e  kombu-5.3.6.tar.gz
"
