# Contributor: Jakub Jirutka <jakub@jirutka.cz>
# Maintainer: Jakub Jirutka <jakub@jirutka.cz>
pkgname=swc
pkgver=1.4.11
pkgrel=0
pkgdesc="A super-fast TypeScript / JavaScript compiler written in Rust"
url="https://swc.rs"
# riscv64: it would take eternity to build
arch="all !riscv64"
license="Apache-2.0"
makedepends="cargo cargo-auditable"
source="https://github.com/swc-project/swc/archive/v$pkgver/swc-$pkgver.tar.gz"
builddir="$srcdir/$pkgname-$pkgver/bindings"
# !check: TODO: run tests
# net: fetch dependencies
options="!check net"

prepare() {
	default_prepare

	# This is unwanted and breaks build on ARM.
	rm ../.cargo/config.toml

	cargo fetch --target="$CTARGET" --locked
}

build() {
	cargo auditable build -p swc_cli --release --locked
}

package() {
	install -D -m755 target/release/swc -t "$pkgdir"/usr/bin/
}

sha512sums="
1fe3b3ece86ade5ec586e591e4a89ede5496c0cffcd00245f973b9df7763646c184b1758928d0939ce0c708d47e6fd7c1c02f5c19de89eb29741ac636a1c8a86  swc-1.4.11.tar.gz
"
